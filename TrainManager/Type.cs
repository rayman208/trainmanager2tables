﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainManager
{
    class Type
    {
        public int Id { set; get; }
        public string Name { get; set; }
    }
}
